public enum Example2HashProto : ushort
{
    /// <summary>name</summary>
    CONFIG_NAME = 4096,
    /// <summary>number</summary>
    CONFIG_NUMBER,
    /// <summary>account</summary>
    USERINFO_ACCOUNT = 5096,
    /// <summary>password</summary>
    USERINFO_PASSWORD,
    /// <summary>moveSpeed</summary>
    USERINFO_MOVESPEED,
    /// <summary>position</summary>
    USERINFO_POSITION,
    /// <summary>rotation</summary>
    USERINFO_ROTATION,
    /// <summary>health</summary>
    USERINFO_HEALTH,
    /// <summary>healthMax</summary>
    USERINFO_HEALTHMAX,
    /// <summary>buffer</summary>
    USERINFO_BUFFER,
}
